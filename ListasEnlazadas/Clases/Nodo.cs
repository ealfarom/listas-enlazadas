﻿using System;


namespace ListasEnlazadas
{
    class Nodo
    {

        private int dato;
        private Nodo siguiente;

        //constructor
        public int Dato
        {
            get { return dato; }
            set { dato = value; }
        }

        //constructor
        public Nodo Siguiente
        {
            get { return siguiente; }
            set { siguiente = value; }
        }

    }
}
