﻿using System;
using System.Windows.Forms;

namespace ListasEnlazadas
{
    class ControlNodos
    {
        //creamos 2 instancias del tipo Nodo
        private Nodo Primero = new();
        private Nodo Ultimo = new ();

        //constructor de ControlNodos
        public ControlNodos()
        {
            Primero = null;
            Ultimo = null;
        }

        //Funcion de tipo void para insertar valor en Los Nodos
        public void InsertarDatos(int valor)
        {
            //creamos un objeto de tipo nodo
            Nodo nuevo = new();
            
            //el parametro valor lo asignamos a dato
            nuevo.Dato = valor ;

            //cabeza de nuestra lista de nodos
            if(Primero == null)
            {
                Primero = nuevo;
                Primero.Siguiente = null;
                Ultimo = nuevo;
            }//cuerpo de nuestros nodos
            else
            {
                Ultimo.Siguiente = nuevo;
                nuevo.Siguiente = null;
                Ultimo = nuevo;
            }
            //mensaje de nodo insertado correctamente
            MessageBox.Show("Dato Ingresado");
        }

        //Funcion Para Listar nuestros nodos
        public void ListarNodos()
        {
            //creamos un objeto de tipo nodo
            Nodo actual = new();
            actual = Primero;
            string colleccionLista = "";

            //hay nodos dentro de la lista si retorna diferente de null
            if (Primero != null)
            {
                //recorremos el nodo actual
                while (actual!=null)
                {
                    //variable de tipo string que asignamos el valor del siguiente
                    string valorApuntado = "";
                    //si hay valor hacia adelante
                    if (actual.Siguiente!=null)
                    {
                        //muestra el valor hacia adelante
                        valorApuntado = $"{actual.Siguiente.Dato}";
                    }
                    //agregar a un string el valor del nodo actual y a cual apunta
                    colleccionLista+= $"{actual.Dato} apunta a -> {valorApuntado}\n";
                    //asigna al nodo actual ucal es el siguiente
                    actual = actual.Siguiente;
                }
                //despliega el valor de los nodos almacenados en coleccion lista
                MessageBox.Show(colleccionLista);
            }
            else//no encuentra valores en la lista
            {
                MessageBox.Show("la lista esta vacia!");
            }
        }

    }
}
