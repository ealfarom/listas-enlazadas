﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListasEnlazadas
{
    public partial class MenuPrincipal : Form
    {
        //iniciamos la clase de Control de nodos para poder acceder desde aca
        ControlNodos control = new ControlNodos();

        public MenuPrincipal()
        {
            InitializeComponent();
        }

        private void MenuPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            //obtenemos el valor en el campo txtValor
            int valor = int.Parse(txtValor.Text);
            //limpiamos el campo 
            txtValor.Text = "";
            
            //enviamos a la clase control el valor del dato
            control.InsertarDatos(valor);
            
        }

        private void btnVer_Click(object sender, EventArgs e)
        {
            //llamamos la funcion de la clase Control ListarNodos
            control.ListarNodos();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            //con este comando cerramos la aplicacion
            Application.Exit();
        }
    }
}
